﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TaskBoard.Master" AutoEventWireup="true" CodeBehind="TaskBoard.aspx.cs" Inherits="TaskBoard.Web.Pages.TaskBoard" %>

<%@ Register Src="~/Controls/Column/Column.ascx" TagPrefix="uc1" TagName="Column" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script src="../Scripts/TaskTableScripts.js"></script>
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row justify-content-center">


        <asp:Button runat="server" ID="SaveButton" class="btn btn-outline-dark" Text="Save all changes" OnClick="SaveButton_Click" />
        <asp:Button runat="server" ID="Refresh" class="btn btn-outline-dark" Text="Refresh board" OnClick="Refresh_Click" />

    </div>
    <div class="row">
        <uc1:Column runat="server" ID="ColumnHigh" priority="High" />
        <uc1:Column runat="server" ID="ColumnNormal" priority="Normal" />
        <uc1:Column runat="server" ID="ColumnLow" priority="Low" />
    </div>

    <asp:ObjectDataSource runat="server" ID="HighPriorityDataSource" TypeName="TaskBoard.Web.Services.TaskBoardService" SelectMethod="GetHighPriorityTickets" />
    <asp:ObjectDataSource runat="server" ID="NormalPriorityDataSource" TypeName="TaskBoard.Web.Services.TaskBoardService" SelectMethod="GetNormalPriorityTickets" />
    <asp:ObjectDataSource runat="server" ID="LowPriorityDataSource" TypeName="TaskBoard.Web.Services.TaskBoardService" SelectMethod="GetLowPriorityTickets" />

</asp:Content>