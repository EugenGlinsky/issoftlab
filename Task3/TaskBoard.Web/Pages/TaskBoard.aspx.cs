﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using TaskBoard.Web.Models;
using TaskBoard.Web.Presenters;
using TaskBoard.Web.Services;

namespace TaskBoard.Web.Pages
{
    public partial class TaskBoard : System.Web.UI.Page
    {
        ITaskBoardPresenter _presenter;
        public TaskBoard()
        {
            _presenter = new TaskBoardPresenter(new TaskBoardService());
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                SetDataSource();
            }
        }

        protected void SaveButton_Click(object sender, EventArgs e)
        {
            var tickets = new List<TicketModel>();
            tickets.AddRange(ColumnHigh.Tickets);
            tickets.AddRange(ColumnLow.Tickets);
            tickets.AddRange(ColumnNormal.Tickets);
            _presenter.SaveTicketsToDb(tickets);
            SetDataSource();
        }

        protected void Refresh_Click(object sender, EventArgs e)
        {
            Server.Transfer("TaskBoard.aspx");
        }

        private void SetDataSource()
        {
            ColumnHigh.SetDataSource(HighPriorityDataSource);
            ColumnHigh.DataBind();
            ColumnLow.SetDataSource(LowPriorityDataSource);
            ColumnLow.DataBind();
            ColumnNormal.SetDataSource(NormalPriorityDataSource);
            ColumnNormal.DataBind();
        }
    }
}