﻿using System.Collections.Generic;
using TaskBoard.Web.Models;

namespace TaskBoard.Web.Presenters
{
    public interface ITaskBoardPresenter
    {
        void SaveTicketsToDb(IEnumerable<TicketModel> tickets);


    }
}
