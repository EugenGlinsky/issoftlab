﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TaskBoard.Web.Models;
using TaskBoard.Web.Repository;
using TaskBoard.Web.Services;

namespace TaskBoard.Web.Presenters
{
    public class TaskBoardPresenter : ITaskBoardPresenter
    {
        private readonly ITaskBoardService _service;

        public TaskBoardPresenter(ITaskBoardService service)
        {
            _service = service;

        }


        public void SaveTicketsToDb(IEnumerable<TicketModel> tickets)
        {
            var dbModels = ConvertToDbModels(tickets);
            _service.SaveTicketsToDb(dbModels);

        }

        private List<TicketDataModel> ConvertToDbModels(IEnumerable<TicketModel> dataModels)
        {

            var collection = new List<TicketDataModel>();
            foreach (var item in dataModels)
            {
                var currentModel = new TicketDataModel()
                {

                    Category = item.Category,
                    CreateDate = item.CreateDate,
                    Name = item.Name,
                    Priority = (int)item.Priority,
                    Id = item.Id

                };
                collection.Add(currentModel);

            }
            return collection;
        }

    }
}