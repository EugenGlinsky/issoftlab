﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskBoard.Web.Models;
using TaskBoard.Web.Repository;

namespace TaskBoard.Web.Services
{
    public interface ITaskBoardService
    {
        void SaveTicketsToDb(List<TicketDataModel> dataModels);
    }
}