﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TaskBoard.Web.Models;
using TaskBoard.Web.Repository;

namespace TaskBoard.Web.Services
{
    public class TaskBoardService : ITaskBoardService
    {
        private IStorage _storage;


        public TaskBoardService()
        {
            _storage = new DataStorage();

        }


        public IEnumerable<TicketModel> GetHighPriorityTickets()
        {

            var highPriorityTickets = _storage.GetTicketsByPriorityTickets(PriorityEnum.High);
            return ConvertToViewModel(highPriorityTickets);
        }

        public IEnumerable<TicketModel> GetLowPriorityTickets()
        {
            var lowPriorityTickets = _storage.GetTicketsByPriorityTickets(PriorityEnum.Low);
            return ConvertToViewModel(lowPriorityTickets);
        }

        public IEnumerable<TicketModel> GetNormalPriorityTickets()
        {
            var normalPriorityTickets = _storage.GetTicketsByPriorityTickets(PriorityEnum.Normal);
            return ConvertToViewModel(normalPriorityTickets);
        }

        public void SaveTicketsToDb(List<TicketDataModel> dataModels)
        {
            _storage.SaveTicketsToDb(dataModels);
        }

        public IEnumerable<PriorityEnum> GetAllPriorities()
        {
            return Enum.GetValues(typeof(PriorityEnum)).Cast<PriorityEnum>();
        }


        private IEnumerable<TicketModel> ConvertToViewModel(IEnumerable<TicketDataModel> dataModels)
        {

            var collection = new List<TicketModel>();
            foreach (var item in dataModels)
            {
                var currentModel = new TicketModel()
                {
                    Category = item.Category,
                    CreateDate = item.CreateDate,
                    Name = item.Name,
                    Priority = (PriorityEnum)item.Priority,
                    Id = item.Id
                };
                collection.Add(currentModel);

            }
            return collection;
        }

    }
}