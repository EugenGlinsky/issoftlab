﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TaskBoard.Web.Models;

namespace TaskBoard.Web.Controls
{
    public partial class Column : System.Web.UI.UserControl
    {
        public List<TicketModel> Tickets { get; set; }

        public PriorityEnum priority { get; set; }

       

        public Column()
        {
            Tickets = new List<TicketModel>();
            
        }


        public void SetDataSource(IDataSource dataSource)
        {
            ListView.DataSource = dataSource ?? throw new ArgumentNullException(nameof(dataSource));
            
            ListView.DataBind();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if(Page.IsPostBack)
            {
                Tickets = (List<TicketModel>)ViewState["Tickets"] ?? new List<TicketModel>();
                ListView.DataSource = Tickets;
            }
            else
            {
                var s = ((ObjectDataSource)ListView.DataSource).Select();
                foreach (var item in s)
                {
                    Tickets.Add((TicketModel)item);
                }
            }
            ViewState["Tickets"] = Tickets;
        }

     
        protected void ListView_ItemInserting(object sender, ListViewInsertEventArgs e)
        {
            var newTicket = new TicketModel
            {
                Id=0,
                Category =(string) e.Values["Category"],
                Name=(string)e.Values["Name"],
                CreateDate=DateTime.Now,
                Priority=this.priority
            };
            Tickets.Add(newTicket);

            ViewState["Tickets"] = Tickets;
            ListView.DataBind();
        }

        protected void ListView_ItemCanceling(object sender, ListViewCancelEventArgs e)
        {
            ListView.EditIndex = -1;
            ListView.DataBind();
        }


        protected void ListView_ItemDeleting(object sender, ListViewDeleteEventArgs e)
        {
          var ticketToDelete=  Tickets.Find(t => t.Id == int.Parse(e.Values["Id"].ToString()));
            Tickets.Remove(ticketToDelete);
            ViewState["Tickets"] = Tickets;
            ListView.EditIndex = -1;
            ListView.DataBind();
        }

        protected void ListView_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            ListView.EditIndex = e.NewEditIndex;
            ListView.DataBind();
        }

        protected void ListView_ItemUpdating(object sender, ListViewUpdateEventArgs e)
        {
            var ticket = Tickets.Find(t => t.Id == int.Parse(e.NewValues["Id"].ToString()));
            ticket.Name = e.NewValues["Name"].ToString();
            ticket.Category = e.NewValues["Category"].ToString();
            ticket.Priority = (PriorityEnum)(Enum.Parse(typeof(PriorityEnum), (e.NewValues["Priority"]).ToString()));
            ViewState["Tickets"] = Tickets;
            ListView.EditIndex = -1;
            ListView.DataBind();
        }
    }
}