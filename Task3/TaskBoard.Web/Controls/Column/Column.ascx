﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Column.ascx.cs" Inherits="TaskBoard.Web.Controls.Column" %>

<div class="col-4 " onclick="NewTickedClicked(this, event.target)">
    <div class="row justify-content-center">
        <button type="button" class="btn btn-outline-dark add-newticket-button ">Add ticket</button>
    </div>
    <div class="row justify-content-center">
        <h3>
            <span class="badge badge-pill badge-secondary"></span>
        </h3>

        <h4>
            <span class="badge badge-pill badge-secondary">Priority: <%=priority %></span>
        </h4>
    </div>

    <div class="row justify-content-center">
        <asp:ListView runat="server" ID="ListView" InsertItemPosition="FirstItem"
            OnItemInserting="ListView_ItemInserting"
            OnItemCanceling="ListView_ItemCanceling"
            OnItemDeleting="ListView_ItemDeleting"
            OnItemEditing="ListView_ItemEditing"
            OnItemUpdating="ListView_ItemUpdating"
           > 
            <ItemTemplate>
                <div class="ticket col-sm-10 row justify-content-center ">
                    <div class="card col-sm-10 text-white bg-dark mb-3" style="max-width: 18rem;">
                        <div class="card-body ">
                            <asp:HiddenField runat="server" ID="idfield" Value='<%#Bind("Id") %>' />
                            <label>Name</label>
                            <asp:TextBox runat="server" ID="NameTextBox" CssClass="form-control input-element"  Text='<%# Bind("Name") %>' Enabled="false" />

                            <label>Category</label>
                            <asp:TextBox runat="server" ID="CategoryTextBox" CssClass="form-control input-element"  Text='<%# Bind("Category") %>' Enabled="false" />

                            <br />
                            <label class="mr-sm-2 onlyOnEdit " for="inlineFormCustomSelect">Priority </label>

                            <asp:TextBox runat="server" ID="PriorityTextBox" CssClass="form-control input-element" Text='<%# Bind("Priority") %>' Enabled="false"/>
                            

                            <h6>Create Date</h6>
                            <h6>
                                <asp:Label runat="server" ID="CreateDateTextBox"  Text='<%# Bind("CreateDate") %>'></asp:Label>
                            </h6>
                            <ul class="nav nav-tabs card-header-tabs">
                                <li class="nav-item">
                                    <asp:Button  class="nav-link bg-dark edit-btn " CommandName="Edit"
                                        Text="Edit" runat="server" />
                                </li>
                                <li class="nav-item">
                                    <asp:Button  class="nav-link bg-dark edit-btn" CommandName="Delete"
                                        Text="Delete" runat="server" />
                                </li>
                            </ul>
                        </div>
                    </div>
                    <br />
                </div>
            </ItemTemplate>
            <EditItemTemplate>
                <div class="ticket col-sm-10 row justify-content-center ">
                    <div class="card text-white bg-dark mb-3" style="max-width: 18rem;">
                        <div class="card-body ">
                            <asp:HiddenField runat="server" ID="idfield" Value='<%#Bind("Id") %>'/>
                            <p>Name</p>
                            <asp:TextBox runat="server" CssClass="form-control input-element" ID="NameTxtBox" Text='<%# Bind("Name") %>' />

                            <p>Category</p>
                            <asp:TextBox runat="server" CssClass="form-control input-element" ID="CategoryTxtBox" Text='<%# Bind("Category") %>' />
                            <p>Priority</p>
                            <asp:DropDownList runat="server" ID="PriorityDropDown" CssClass="form-control input-element" DataSourceID="Priorities" Text='<%# Bind("Priority") %>' >
                              
                            </asp:DropDownList>
                           
                            <ul class="nav nav-tabs card-header-tabs">
                                <li class="nav-item">
                                    <asp:Button ID="SaveButton" class="nav-link bg-dark edit-btn " CommandName="Update"
                                        Text="Save" runat="server" />
                                </li>
                                <li class="nav-item">
                                    <asp:Button ID="CancelButton" class="nav-link bg-dark edit-btn" CommandName="Cancel"
                                        Text="Cancel" runat="server" />
                                </li>
                            </ul>
                        </div>
                    </div>
                    <br />
                </div>
            </EditItemTemplate>
            <InsertItemTemplate>
                <div class="ticket col-sm-10 row justify-content-center  templateTicket" style="display: none">
                    <div class="card text-white bg-dark mb-3" style="max-width: 18rem;">
                        <div class="card-body ">
                            <asp:HiddenField runat="server" ID="idfield" Value='<%#Eval("Id") %>' />
                            <p>Name</p>
                            <asp:TextBox runat="server" CssClass="form-control input-element" ID="NameTxtBox" Text='<%# Bind("Name") %>' />

                            <p>Category</p>
                            <asp:TextBox runat="server" CssClass="form-control input-element" ID="CategoryTxtBox" Text='<%# Bind("Category") %>' />
                           
                            
                            <ul class="nav nav-tabs card-header-tabs">
                                <li class="nav-item">
                                    <asp:Button ID="SaveButton" class="nav-link bg-dark edit-btn " CommandName="Insert"
                                        Text="Save" runat="server" />
                                </li>
                                <li class="nav-item">
                                    <asp:Button ID="DeleteButton" class="nav-link bg-dark edit-btn" CommandName="Cancel"
                                        Text="Cancel" runat="server" />
                                </li>
                            </ul>
                        </div>
                    </div>
                    <br />
                </div>
            </InsertItemTemplate>
            
            <EmptyDataTemplate>
                <table class="table" runat="server">
                    <tr>
                        <td>No data.</td>
                    </tr>
                </table>
            </EmptyDataTemplate>
        </asp:ListView>

    </div>
</div>
<asp:ObjectDataSource runat="server" Id="Priorities" TypeName="TaskBoard.Web.Services.TaskBoardService" SelectMethod="GetAllPriorities" />
