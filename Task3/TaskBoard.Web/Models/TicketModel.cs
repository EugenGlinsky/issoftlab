﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TaskBoard.Web.Models
{
    [Serializable]
    public class TicketModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Category { get; set; }

        public DateTime CreateDate { get; set; }

        public PriorityEnum Priority { get; set; }
    }
}