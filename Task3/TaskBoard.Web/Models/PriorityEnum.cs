﻿namespace TaskBoard.Web.Models
{
    public enum PriorityEnum
    {
        Low = 0,
        Normal = 1,
        High = 2
    }

}