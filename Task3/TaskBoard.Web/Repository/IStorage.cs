﻿using System.Collections.Generic;
using TaskBoard.Web.Models;

namespace TaskBoard.Web.Repository
{
    interface IStorage
    {
        IEnumerable<TicketDataModel> GetTicketsByPriorityTickets(PriorityEnum priority);

        void SaveTicketsToDb(List<TicketDataModel> ticketCollection);

    }
}
