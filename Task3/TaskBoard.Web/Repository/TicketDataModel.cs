﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TaskBoard.Web.Repository
{
    public class TicketDataModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Category { get; set; }

        public DateTime CreateDate { get; set; }

        public int Priority { get; set; }

    }
}