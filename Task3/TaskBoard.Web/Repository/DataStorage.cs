﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using TaskBoard.Web.Models;

namespace TaskBoard.Web.Repository
{
    public class DataStorage : IStorage
    {
        private string Puth = HostingEnvironment.ApplicationPhysicalPath + @"./Tickets.json";

        private Dictionary<int,TicketDataModel> _allTickets;


        public DataStorage()
        {
            _allTickets = GetTicketsFromDb();
        }

        public IEnumerable<TicketDataModel> GetTicketsByPriorityTickets(PriorityEnum priority)
        {
            return _allTickets.Where(t => t.Value.Priority == (int)priority).Select(e => e.Value).ToList<TicketDataModel>();
           
        }

        public Dictionary<int,TicketDataModel> GetTicketsFromDb()
        {
            string text;
            var fileStream = new FileStream(Puth, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            using (var streamReader = new StreamReader(fileStream))
            {
                text = streamReader.ReadToEnd();
            }

            var tickets = JsonConvert.DeserializeObject<Dictionary<int,TicketDataModel>>(text);

            return tickets ?? new Dictionary<int, TicketDataModel>();
        }

        public void SaveTicketsToDb(List<TicketDataModel> ticketCollection)
        {

            var maxId = _allTickets.Keys.Count>0 ? _allTickets.Keys.Max() : 0 ;
            var tmpTickets = new Dictionary<int, TicketDataModel>();

            foreach (var ticket in ticketCollection)
            {
                if(_allTickets.Keys.Contains(ticket.Id))
                {
                    tmpTickets.Add(ticket.Id, ticket);
                }
                else
                {
                    ticket.Id = ++maxId;
                    tmpTickets.Add(ticket.Id, ticket);
                }
            }

            string text = JsonConvert.SerializeObject(tmpTickets);

            try
            {
                File.WriteAllText(Puth, text);
            }
            catch
            {
                throw;
            }
        }
    }
}