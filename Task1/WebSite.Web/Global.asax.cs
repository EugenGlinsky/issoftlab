﻿using System;
using System.Web.Optimization;
using WebSite.Web.App_Start;

namespace WebSite.Web
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}