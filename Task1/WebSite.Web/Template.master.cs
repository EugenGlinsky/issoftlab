﻿using System;

namespace WebSite.Web
{
    public partial class Template : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Cookies["IsAuth"].Value == "false" || Request.Cookies== null)
            {
                Response.Redirect("~/Pages/LoginPage.aspx");
            }
        }
    }
}