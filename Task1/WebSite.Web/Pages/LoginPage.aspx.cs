﻿using System;
using System.Configuration;
using System.Web;
using WebSite.Web.Models;

namespace WebSite.Web
{
    public partial class LoginPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
                Response.Cookies["IsAuth"].Value = "false";
        }

        protected void ButtonSubmit_Pressed(object sender, EventArgs e)
        {
            var CurrentUser = new User
            {
                Email = EmailTxtBox.Text,
                Password = PasswordTxtBox.Text
            };

            var adminLogin = ConfigurationManager.AppSettings.Get("Login");
            var adminPassword = ConfigurationManager.AppSettings.Get("Password");
            var IsLoginCorrect = String.Equals(CurrentUser.Email, adminLogin, StringComparison.OrdinalIgnoreCase);
            var IsPasswordCorrect = String.Equals(CurrentUser.Password, adminPassword, StringComparison.Ordinal);

            if (IsLoginCorrect && IsPasswordCorrect)
            {
                var cookie = new HttpCookie("IsAuth");
                cookie.Value = "true";
                cookie.Secure = true;
                Response.Cookies.Add(cookie);
                Response.Redirect("~/Pages/HomePage.aspx");
            }
        }
    }
}