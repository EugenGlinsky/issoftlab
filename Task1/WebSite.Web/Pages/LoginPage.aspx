﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="LoginPage.aspx.cs" Inherits="WebSite.Web.LoginPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div class="container">
        <div class="row">
            <div class="col-sm-9 col-md-7 col-lg-5 mx-auto ">
                <div class="card card-signin my-5 ">
                    <div class="card-body">
                        <h5 class="card-title text-center">Sign In</h5>
                        <br />
                        <asp:TextBox runat="server" ID="EmailTxtBox" type="" CssClass="form-control" TextMode="Email" placeholder="Email Address" required="" autofocus="" />
                        <br />
                        <asp:TextBox runat="server" ID="PasswordTxtBox"  TextMode="Password" CssClass="form-control" name="password" placeholder="Password" required="" />
                        <br />
                        <asp:Button runat="server" CssClass="btn btn-lg btn-primary btn-block" type="submit" Text="Login"  OnClick="ButtonSubmit_Pressed"  />
                        <br />
                        <h5>Login: admin@gmail.com </h5>
                        <h5>Password: admin</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
