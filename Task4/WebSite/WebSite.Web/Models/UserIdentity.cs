﻿using System.Security.Principal;
using WebSite.Web.Models.ViewModels;

namespace WebSite.Web.Models
{
    public class UserIdentity : IIdentity
    {
        public string Name { get;  }

        public string AuthenticationType => typeof(User).ToString();

        public bool IsAuthenticated { get;  }


        public UserIdentity(string name)
        {
            Name = name;
            IsAuthenticated = true;
        }
    }
}