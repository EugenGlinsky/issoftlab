﻿using System;
using System.Security.Principal;
using WebSite.Web.Services;

namespace WebSite.Web.Models
{
    public class UserPrincipal : IPrincipal
    {
        public IIdentity Identity { get; }

        public Guid UserId { get; set; }

        public UserPrincipal(string name, Guid id)
        {
            Identity = new UserIdentity(name);
            UserId = id;
        }

        public bool IsInRole(string role)
        {
            var service = new UserService();
            var userRole = service.GetUserRole(Identity.Name);
            return role.Equals(userRole.ToString(), StringComparison.CurrentCultureIgnoreCase);
        }
    }
}