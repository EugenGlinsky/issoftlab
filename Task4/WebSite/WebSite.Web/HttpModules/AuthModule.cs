﻿using System;
using System.Web;
using System.Web.Security;
using WebSite.Web.Models;

namespace WebSite.Web.HttpModules
{
    public class AuthModule : IHttpModule
    {
        public void Init(HttpApplication context)
        {
            context.AuthenticateRequest += SetIdentity;
        }

        public void Dispose()
        {

        }

        private void SetIdentity(object sender, EventArgs args)
        {
            var authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];

            if (authCookie == null) return;

            var authTicket = FormsAuthentication.Decrypt(authCookie.Value);

            if (authTicket == null) return;

            var id = Guid.Parse(authTicket.UserData);

            var userName = authTicket.Name;

            var newUser = new UserPrincipal(userName, id) ;

            HttpContext.Current.User = newUser;
        }
    }
}