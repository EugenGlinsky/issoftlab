﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebSite.Web.General;
using WebSite.Web.Models.ViewModels;

namespace WebSite.Web.Dependences
{
    interface IUserService
    {

        IEnumerable<User> GetAllUsers();

        User GetUserByName(string name);

        User GetUserById(Guid id);

        Role GetUserRole(string userName);

        Status CreateUser(string name, string password);

        Status CreateUser(User user);

        Status DeleteUser(string name);

        Status ChangeEmail(string userName, string email);

        Status ChangeRole(string userName, Role role);

        bool UserInRole(string userName, string role);


    }
}
