﻿using System.Collections.Generic;
using WebSite.Web.Models.ViewModels;

namespace WebSite.Web.Dependences
{
    public interface IStorage
    {
        IEnumerable<User> GetAllUsers();

        void SaveAllUsers(IEnumerable<User> users);

    }
}
