﻿using System;
using System.Configuration;
using System.Web;
using WebSite.Web.Models.ViewModels;
using WebSite.Web.Services;

namespace WebSite.Web
{
    public class Global : HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            var username = ConfigurationManager.AppSettings["AdminUserName"];
            var password = ConfigurationManager.AppSettings["AdminPassword"];
            var email = ConfigurationManager.AppSettings["AdminEmail"];
            var service = new UserService();

            if (service.GetUserByName(username) == null)
                service.CreateUser(new User { Name = username, Password = password, Email=email, Role = Role.Admin });
        }
    }
}