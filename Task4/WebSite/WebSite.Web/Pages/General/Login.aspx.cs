﻿using System;
using System.Web.UI;
using WebSite.Web.Services;

namespace WebSite.Web.Pages.General
{
    public partial class Login : Page
    {
        protected void Unnamed_Click(object sender, EventArgs e)
        {
            Page.Validate();

            if (!Page.IsValid) return;

            var login = LoginTxtBox.Text;
            var password = PasswordTxtBox.Text;
            if (AuthService.AuthUser(login, password))
            {
                Response.Redirect("~/Pages/Users/HomePage.aspx");
            }
            else
            {
                Response.Write("No user with this credentials");
            }
        }
    }
}