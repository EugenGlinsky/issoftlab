﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/WebSite.Master" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="WebSite.Web.Pages.Register" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <div class="container">
        <div class="row">
            <div class="col-sm-9 col-md-7 col-lg-5 mx-auto ">
                <div class="card card-signin my-5 ">
                    <div class="card-body">
                        <h5 class="card-title text-center">Sign In</h5>
                        <br />
                        <div>
                            <asp:ValidationSummary ID="ValidatorSummary" runat="server" CssClass="text-danger" ValidationGroup="allFields" ShowSummary="true" DisplayMode="BulletList" />
                        </div>
                        <asp:TextBox runat="server" ID="LoginTxtBox" type="" CssClass="form-control" placeholder="Login" required="" autofocus="" />
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="LoginTxtBox"  Display="None" ValidationGroup="allFields" ErrorMessage="Login is required"> </asp:RequiredFieldValidator>
                        <br />
                        <asp:TextBox runat="server" ID="PasswordTxtBox" TextMode="Password" CssClass="form-control" name="password" placeholder="Password" required="" />
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="PasswordTxtBox"  Display="None" ValidationGroup="allFields" ErrorMessage="Password is required"> </asp:RequiredFieldValidator>
                        <br />
                        <asp:TextBox runat="server" ID="EmailTextBox" TextMode="Email" CssClass="form-control" name="password" placeholder="Email" required="" />
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="EmailTextBox"  Display="None" ValidationGroup="allFields" ErrorMessage="Email is required"> </asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator runat="server" ControlToValidate="EmailTextBox" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ErrorMessage="Invalid format Email" ValidationGroup="allFields" Display="None"></asp:RegularExpressionValidator>
                        <br />
                        <asp:Button runat="server" CssClass="btn btn-lg btn-dark btn-block" type="submit" Text="Register" CausesValidation="true" ValidationGroup="allFields" OnClick="Unnamed_Click" />
                        <br />
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
