﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/WebSite.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="WebSite.Web.Pages.General.Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    
    <div class="container">
        <div class="row">
            <div class="col-sm-9 col-md-7 col-lg-5 mx-auto ">
                <div class="card card-signin my-5 ">
                    <div class="card-body">
                        <h5 class="card-title text-center">Sign In</h5>
                        <br />
                        <asp:TextBox runat="server" ID="LoginTxtBox" type="" CssClass="form-control" placeholder="Login" required="" autofocus="" />
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="LoginTxtBox"  Display="None" ValidationGroup="allFields"> </asp:RequiredFieldValidator>
                        <br />
                        <asp:TextBox runat="server" ID="PasswordTxtBox" TextMode="Password" CssClass="form-control" name="password" placeholder="Password" required="" />
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="PasswordTxtBox"  Display="None" ValidationGroup="allFields"> </asp:RequiredFieldValidator>
                        <br />
                        <asp:Button runat="server" CssClass="btn btn-lg btn-dark btn-block" type="submit" Text="Login" CausesValidation="true" ValidationGroup="allFields"  OnClick="Unnamed_Click"/>
                        <br />
                        <br>
                        <asp:HyperLink runat="server" NavigateUrl="~/Pages/General/Register.aspx" CssClass="btn btn-lg btn-dark btn-block" Text="Register"></asp:HyperLink>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
