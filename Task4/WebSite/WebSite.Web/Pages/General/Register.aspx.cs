﻿using System;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebSite.Web.General;
using WebSite.Web.Models.ViewModels;
using WebSite.Web.Services;

namespace WebSite.Web.Pages
{
    public partial class Register : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }


        protected void Unnamed_Click(object sender, EventArgs e)
        {
            Page.Validate();
            if (!Page.IsValid) return;

            var login = LoginTxtBox.Text;
            var password = PasswordTxtBox.Text;
            var email = EmailTextBox.Text;

            var service = new UserService();
            var status = service.CreateUser(new User { Email = email, Name = login, Password = password, RegistrationDate = DateTime.Now, Role = Role.Admin });
            if (status == Status.Success)
            {
                AuthService.AuthUser(login, password);
            }
            else
            {
                Response.Write(status.ToString());
            }
        }
    }
}