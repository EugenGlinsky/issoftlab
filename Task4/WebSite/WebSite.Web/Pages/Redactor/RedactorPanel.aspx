﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/WebSite.Master" AutoEventWireup="true" CodeBehind="RedactorPanel.aspx.cs" Inherits="WebSite.Web.Pages.Redactor.RedactorPanel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:ListView runat="server" ID="ListView"
        EnableViewState="true"
                  OnItemEditing="ListView_ItemEditing"
                  OnItemCanceling="ListView_ItemCanceling"
                  OnItemUpdating="ListView_ItemUpdating">
        <LayoutTemplate>
            <table class="table" runat="server">
                <tr runat="server">
                    <td runat="server">
                        <table id="itemPlaceholderContainer" class="table table-hover" runat="server">
                            <tr runat="server">
                                <th class="w-25" runat="server">Name</th>
                                <th class="w-25" runat="server">Email</th>
                                <th class="w-25" runat="server">Create Date</th>
                                <th class="w-25" runat="server">Role</th>
                                <th runat="server"></th>
                            </tr>
                            <tr id="itemPlaceholder" runat="server">
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </LayoutTemplate>
        <ItemTemplate>
            <tr>
                <td class="w-25">
                    <asp:Label ID="NameLabel" runat="server"
                               Text='<%# Eval("Name") %>' />
                </td>
                <td class="w-25">
                    <asp:Label ID="MailLabel" runat="server"
                               Text='<%# Eval("Email") %>' />
                </td>
                <td class="w-25">
                    <asp:Label ID="DateLabel" runat="server"
                               Text='<%# DataBinder.Eval(Container.DataItem, "RegistrationDate", "{0:MM/dd/yyyy}") %>' />
                </td>
                <td class="w-25">
                    <asp:Label ID="RoleLabel" runat="server"
                               Text='<%# Eval("Role") %>' />
                </td>
                <td class="btn-group">
                    <asp:Button ID="EditButton" CssClass="btn btn-dark" CommandName="Edit"
                                Text="Edit" runat="server" />
                    <asp:Button ID="DeleteButton" CssClass="btn btn-danger" CommandName="Delete"
                                Text="Delete" runat="server" OnClientClick=" return alert('Do you really wont delete this user?');" />
                </td>
            </tr>
        </ItemTemplate>
        <EditItemTemplate>
            <tr>
                <td class="w-25">
                    <asp:Label ID="NameLabel" runat="server"
                               Text='<%# Bind("Name") %>' />
                </td>
                <td class="w-25">
                    <asp:TextBox ID="MailTextBox" CssClass="form-control" runat="server"
                                 Text='<%# Bind("Email") %>' />
                </td>
                <td class="w-25">
                    <asp:Label ID="DateLabel" runat="server"
                               Text='<%# DataBinder.Eval(Container.DataItem, "RegistrationDate", "{0:MM/dd/yyyy}") %>' />
                </td>
                <td class="w-25">
                    <asp:Label ID="RoleLabel" runat="server"
                               Text='<%# Eval("Role") %>' />
                </td>
                <td class="btn-group">

                    <asp:Button ID="InsertButton" CssClass="btn btn-dark" CommandName="Update"
                                Text="Update" runat="server" />
                    <asp:Button ID="CancelButton" CssClass="btn btn-danger" CommandName="Cancel"
                                Text="Cancel" runat="server" />
                </td>
            </tr>
        </EditItemTemplate>
    </asp:ListView>
    <asp:ObjectDataSource ID="UsersDataSource" TypeName="WebSite.Web.Services.UserService" runat="server" SelectMethod="GetUsers"></asp:ObjectDataSource>
</asp:Content>
