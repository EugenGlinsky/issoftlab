﻿using System;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebSite.Web.Pages.Redactor
{
    public partial class RedactorPanel : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack) return;

            ListView.DataSource = UsersDataSource;
            ListView.DataBind();
        }

        protected void ListView_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            ListView.EditIndex = e.NewEditIndex;
            ListView.DataSource = UsersDataSource;
            ListView.DataBind();
        }

        protected void ListView_ItemCanceling(object sender, ListViewCancelEventArgs e)
        {
            ListView.EditIndex = -1;
            ListView.DataSource = UsersDataSource;
            ListView.DataBind();
        }

        protected void ListView_ItemUpdating(object sender, ListViewUpdateEventArgs e)
        {
            var name = (string) e.NewValues["Name"];
            var user = Membership.GetUser(name);
            var oldRole = Roles.GetRolesForUser(name)[0];

            if (user != null)
            {
                user.Email = (string) e.NewValues["Email"];
                Membership.UpdateUser(user);
            }

            Roles.RemoveUserFromRole(name, oldRole);
            var newRole = (string) e.NewValues["Role"];
            Roles.AddUserToRole(name, newRole);

            ListView.EditIndex = -1;
            ListView.DataSource = UsersDataSource;
            ListView.DataBind();
        }
    }
}