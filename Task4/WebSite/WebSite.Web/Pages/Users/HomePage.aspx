﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/WebSite.Master" AutoEventWireup="true" CodeBehind="HomePage.aspx.cs" Inherits="WebSite.Web.Pages.Users.HomPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">

    <div class="container-fluid h-100">
        <div class="row align-items-center h-100">
            <div class="col-sm-12">
                <div class="row justify-content-center">
                    <div class="col-4 text-center">
                        <h2>Information</h2>
                        <asp:LoginView runat="server">
                            <LoggedInTemplate>
                                <h1>Hello <%= User.Identity.Name %> !</h1>
                            </LoggedInTemplate>
                            <RoleGroups>
                                <asp:RoleGroup Roles="admin">
                                    <ContentTemplate>
                                        <h1>Hello Admin !</h1>
                                    </ContentTemplate>
                                </asp:RoleGroup>
                            </RoleGroups>
                        </asp:LoginView>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
