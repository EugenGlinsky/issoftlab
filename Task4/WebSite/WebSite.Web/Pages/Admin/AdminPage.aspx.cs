﻿using System;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebSite.Web.Dependences;
using WebSite.Web.General;
using WebSite.Web.Models.ViewModels;
using WebSite.Web.Services;

namespace WebSite.Web.Pages.Admin
{
    public partial class AdminPage : Page
    {
        protected string _status;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack) return;

            ListView.DataSource = UsersDataSource;

            ListView.DataBind();
        }

        protected void RolesDropDown_Load(object sender, EventArgs e)
        {
            var roles = Enum.GetNames(typeof(Role));

            foreach (var role in roles)
            {
                var item = new ListItem(role, role);
                ((DropDownList)sender).Items.Add(item);
            }
        }

        protected void ListView_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            ListView.EditIndex = e.NewEditIndex;
            ListView.DataSource = UsersDataSource;
            ListView.DataBind();
        }

        protected void ListView_ItemDeleting(object sender, ListViewDeleteEventArgs e)
        {
            var item = ListView.Items[e.ItemIndex];
            var userName = ((Label)item.FindControl("NameLabel")).Text;
            var userRole = ((Label)item.FindControl("RoleLabel")).Text;
            var service = new UserService();
            service.DeleteUser(userName);
            ListView.EditIndex = -1;
            ListView.DataSource = UsersDataSource;
            ListView.DataBind();
        }

        protected void ListView_ItemInserting(object sender, ListViewInsertEventArgs e)
        {
            var name = (string)e.Values["Name"];
            var role = (string)e.Values["Role"];
            var email = (string)e.Values["Email"];
            var password = name + "!123";
            var service = new UserService();

            Status status = service.CreateUser(new User
            {
                Email = email,
                Role = (Role)Enum.Parse(typeof(Role), role),
                Password = password,
                Name = name

            });


            if (status == Status.Success)
            {
               _status = $"User was created with password {password}";
            }
            else
            {
                _status = $"User was not created. _status: {status}";
            }

            ListView.EditIndex = -1;
            ListView.DataSource = UsersDataSource;
            ListView.DataBind();
        }


        protected void ListView_ItemCanceling(object sender, ListViewCancelEventArgs e)
        {
            ListView.EditIndex = -1;
            ListView.DataBind();
        }

        protected void ListView_ItemUpdating(object sender, ListViewUpdateEventArgs e)
        {
            var name = (string)e.NewValues["Name"];
            var role = (string)e.NewValues["Role"];
            var service = new UserService();

            service.ChangeEmail(name,(string)e.NewValues["Email"]);
            service.ChangeRole(name, (Role) Enum.Parse(typeof(Role), role));

            ListView.EditIndex = -1;
            ListView.DataSource = UsersDataSource;
            ListView.DataBind();
        }
    }
}