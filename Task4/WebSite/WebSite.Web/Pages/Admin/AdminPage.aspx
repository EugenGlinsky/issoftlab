﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/WebSite.Master" AutoEventWireup="true" CodeBehind="AdminPage.aspx.cs" Inherits="WebSite.Web.Pages.Admin.AdminPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:Label runat="server" ID="StatusLbl"><%= _status %></asp:Label>
    <asp:ListView runat="server" ID="ListView"
        OnItemEditing="ListView_ItemEditing"
        OnItemDeleting="ListView_ItemDeleting"
        OnItemInserting="ListView_ItemInserting"
        OnItemCanceling="ListView_ItemCanceling"
        OnItemUpdating="ListView_ItemUpdating"
        EnableViewState="true"
        InsertItemPosition="FirstItem"
        ViewStateMode="Enabled">
        <LayoutTemplate>
            <table class="table" runat="server">
                <tr runat="server">
                    <td runat="server">
                        <table id="itemPlaceholderContainer" class="table table-hover" runat="server">
                            <tr runat="server">
                                <th class="w-25" runat="server">Name</th>
                                <th class="w-25" runat="server">Email</th>
                                <th class="w-25" runat="server">Create Date</th>
                                <th class="w-25" runat="server">Role</th>
                                <th class="w-25" runat="server">
                                    <a class="btn btn-outline-dark" onclick="showTemplate();">AddUser</a>
                                </th>
                                <th runat="server"></th>
                            </tr>
                            <tr id="itemPlaceholder" runat="server">
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </LayoutTemplate>
        <ItemTemplate>
            <tr>
                <td class="w-25">
                    <asp:Label ID="NameLabel" runat="server"
                        Text='<%# Eval("Name") %>' />
                </td>
                <td class="w-25">
                    <asp:Label ID="MailLabel" runat="server"
                        Text='<%# Eval("Email") %>' />
                </td>
                <td class="w-25">
                    <asp:Label ID="DateLabel" runat="server"
                        Text='<%# DataBinder.Eval(Container.DataItem, "RegistrationDate", "{0:MM/dd/yyyy}") %>' />
                </td>
                <td class="w-25">
                    <asp:Label ID="RoleLabel" runat="server"
                        Text='<%# Eval("Role") %>' />
                </td>
                <td class="btn-group">
                    <asp:Button ID="EditButton" CssClass="btn btn-dark" CommandName="Edit"
                        Text="Edit" runat="server" />
                    <asp:Button ID="DeleteButton" CssClass="btn btn-danger" CommandName="Delete"
                        Text="Delete" runat="server" OnClientClick=" return alert('Do you really wont delete this user?');" />
                </td>
            </tr>
        </ItemTemplate>
        <EditItemTemplate>
            <tr>
                <td class="w-25">
                    <asp:Label ID="NameLabel" runat="server"
                        Text='<%# Bind("Name") %>' />
                </td>
                <td class="w-25">
                    <asp:TextBox ID="MailTextBox" CssClass="form-control" runat="server"
                        Text='<%# Bind("Email") %>' />
                </td>
                <td class="w-25">
                    <asp:Label ID="DateLabel" runat="server"
                        Text='<%# DataBinder.Eval(Container.DataItem, "RegistrationDate", "{0:MM/dd/yyyy}") %>' />
                </td>
                <td class="w-25">
                    <asp:DropDownList ID="RolesDropDown" CssClass="form-control" runat="server"
                        OnLoad="RolesDropDown_Load"
                        Text='<%# Bind("Role") %>' />
                </td>
                <td class="btn-group">
                    <asp:Button ID="InsertButton" CssClass="btn btn-dark" CommandName="Update"
                        Text="Update" runat="server" />
                    <asp:Button ID="CancelButton" CssClass="btn btn-danger" CommandName="Cancel"
                        Text="Cancel" runat="server" />
                </td>
            </tr>
        </EditItemTemplate>
        <InsertItemTemplate>
            <tr class="newItem" style="display: none">

                <td class="w-25">
                    <asp:TextBox ID="NameTextBox" CssClass="form-control" runat="server" Text='<%# Bind("Name") %>' />
                </td>
                <td class="w-25">
                    <asp:TextBox ID="MailTextBox" CssClass="form-control" runat="server" Text='<%# Bind("Email") %>' />
                </td>
                <td class="w-25"></td>
                <td class="w-25">
                    <asp:DropDownList ID="RolesDropDown" CssClass="form-control" runat="server"
                        OnLoad="RolesDropDown_Load" Text='<%# Bind("Role") %>' />
                </td>
                <td class="btn-group">
                    <asp:Button ID="CancelButton" CssClass="btn btn-warning" CommandName="Insert"
                        Text="Create" runat="server" />
                    <asp:Button ID="Button1" CssClass="btn btn-dark" CommandName="Cancel"
                        Text="Cancel" runat="server" />
                </td>
            </tr>
        </InsertItemTemplate>
    </asp:ListView>

    <asp:ObjectDataSource ID="UsersDataSource" TypeName="WebSite.Web.Services.UserService" runat="server" SelectMethod="GetAllUsers"></asp:ObjectDataSource>

    <script>
        function showTemplate() {

            document.getElementsByClassName("newItem")[0].setAttribute("style", "display:normal");
        }
    </script>
</asp:Content>
