﻿namespace WebSite.Web
{
    public enum Role
    {
        User = 0,
        Admin,
        Redactor
    }
}