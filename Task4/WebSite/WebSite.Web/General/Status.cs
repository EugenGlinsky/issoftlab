﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebSite.Web.General
{
    public enum Status
    {
        Success,
        UserNameAlreadyExist,
        PasswordIsShort,
        ServerError
    }
}