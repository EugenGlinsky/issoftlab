﻿using System.Collections.Generic;
using System.IO;
using System.Web.Hosting;
using Newtonsoft.Json;
using WebSite.Web.Dependences;
using WebSite.Web.Models.ViewModels;

namespace WebSite.Web.Storages
{
    public class LocalFileStorage : IStorage
    {
        private readonly string _puth = HostingEnvironment.ApplicationPhysicalPath + @"./Users.json";

        public IEnumerable<User> GetAllUsers()
        {
            string text;

            var fileStream = new FileStream(_puth, FileMode.OpenOrCreate, FileAccess.ReadWrite);

            using (var streamReader = new StreamReader(fileStream))
            {
                text = streamReader.ReadToEnd();
            }

            var users = JsonConvert.DeserializeObject<IEnumerable<User>>(text);

            return users ?? new List<User>();
        }

        public void SaveAllUsers(IEnumerable<User> users)
        {
           var serializesUsers = JsonConvert.SerializeObject(users);
           try
           {
               File.WriteAllText(_puth, serializesUsers);
           }
           catch
           {
               throw;
           }
        }
    }
}