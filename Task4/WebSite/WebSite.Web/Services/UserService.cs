﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebSite.Web.Dependences;
using WebSite.Web.General;
using WebSite.Web.Models.ViewModels;
using WebSite.Web.Storages;

namespace WebSite.Web.Services
{
    public class UserService : IUserService
    {
        private readonly IStorage _storage;

        public UserService() : this(new LocalFileStorage())
        {
        }

        public UserService(IStorage storage)
        {
            _storage = storage;
        }

        public IEnumerable<User> GetAllUsers()
        {
            return _storage.GetAllUsers();
        }

        public User GetUserByName(string name)
        {
            var users = GetAllUsers();

            return users.FirstOrDefault(u => u.Name == name);
        }

        public User GetUserById(Guid id)
        {
            var users = GetAllUsers();

            return users.FirstOrDefault(u => u.Id == id);
        }

        public Role GetUserRole(string userName)
        {
            var users = GetAllUsers();
            return users.Where(u => u.Name == userName).Select(u => u.Role).First();
        }

        public Status CreateUser(string name, string password)
        {
            if (password.Length < 6) return Status.PasswordIsShort;
            var users = GetAllUsers().ToList();
            Status status;
            if (users.Exists(u => u.Name == name)) return Status.UserNameAlreadyExist;

            users.Add(new User
            { Name = name, Password = password, Id = new Guid(), RegistrationDate = DateTime.Now, Role = Role.User });

            try
            {
                _storage.SaveAllUsers(users);
                status = Status.Success;
            }
            catch
            {
                status = Status.ServerError;
            }

            return status;
        }

        public Status CreateUser(User user)
        {
            if (user.Password.Length < 6) return Status.PasswordIsShort;
            var users = GetAllUsers().ToList();
            Status status;
            if (users.Exists(u => u.Name == user.Name)) return Status.UserNameAlreadyExist;

            user.Id = new Guid();
            user.RegistrationDate = DateTime.Now;

            users.Add(user);

            try
            {
                _storage.SaveAllUsers(users);
                status = Status.Success;
            }
            catch
            {
                status = Status.ServerError;
            }

            return status;
        }

        public Status DeleteUser(string name)
        {
            var users = GetAllUsers().ToList();
            var userForDelete = users.FirstOrDefault(u => u.Name == name);
            Status status;

            if (userForDelete == null) return Status.ServerError;

            users.Remove(userForDelete);

            try
            {
                _storage.SaveAllUsers(users);
                status = Status.Success;
            }
            catch
            {
                status = Status.ServerError;
            }

            return status;
        }

        public Status ChangeEmail(string userName, string email)
        {
            var users = GetAllUsers().ToList();
            var userForEdit = users.FirstOrDefault(u => u.Name == userName);
            Status status;
            if (userForEdit != null) userForEdit.Email = email;

            try
            {
                _storage.SaveAllUsers(users);
                status = Status.Success;
            }
            catch
            {
                status = Status.ServerError;
            }

            return status;
        }

        public Status ChangeRole(string userName, Role role)
        {
            var users = GetAllUsers().ToList();
            var userForEdit = users.FirstOrDefault(u => u.Name == userName);
            Status status;
            if (userForEdit != null) userForEdit.Role = role;

            try
            {
                _storage.SaveAllUsers(users);
                status = Status.Success;
            }
            catch
            {
                status = Status.ServerError;
            }

            return status;
        }

        public bool UserInRole(string userName, string role)
        {
            var users = GetAllUsers().ToList();
            return users.Exists(u => u.Name == userName && u.Role.ToString() == role);
        }
    }
}