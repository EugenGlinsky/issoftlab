﻿using System;
using System.Web;
using System.Web.Security;

namespace WebSite.Web.Services
{
    public static class AuthService
    {
        private static UserService _service;

        static AuthService()
        {
            _service = new UserService();
        }

        public static bool AuthUser(string name, string password)
        {
            var user = _service.GetUserByName(name);
            if (user == null) return false;
            if (user.Password != password) return false;

            var userData = user.Id.ToString();
            
            DateTime expiration = DateTime.Now.AddSeconds(FormsAuthentication.Timeout.TotalSeconds);
            var newTicket = new FormsAuthenticationTicket(1, user.Name, DateTime.Now,
                expiration, false, userData, FormsAuthentication.FormsCookiePath);

            var encTicket = FormsAuthentication.Encrypt(newTicket);

            var httpCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);

            HttpContext.Current.Response.Cookies.Add(httpCookie);

            return true;
        }
    }
}