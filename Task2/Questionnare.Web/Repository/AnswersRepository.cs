﻿using Newtonsoft.Json;
using Questionnare.Web.Models;
using System.Collections.Generic;
using System.IO;
using System.Web.Hosting;

namespace Questionnare.Web.Repository
{
    public class AnswersRepository
    {
        private string Puth = HostingEnvironment.ApplicationPhysicalPath + @"./Answers.json";

        public bool Save(Answers answer)
        {
            try
            {
                var answers = GetAllAnswers();
                if (answers == null)
                {
                    answers = new List<Answers>();

                }
                answers.Add(answer);

                string serializedAnswers = JsonConvert.SerializeObject(answers);
                //var fileStream = new FileStream(Puth, FileMode.OpenOrCreate, FileAccess.ReadWrite);

                //using (var streamWriter = new StreamWriter(fileStream))
                //{
                //    streamWriter.Write(serializedAnswers);
                //}

                File.WriteAllText(Puth, serializedAnswers);
                return true;
            }
            catch
            {
                throw;
            }

        }

        public List<Answers> GetAllAnswers()
        {
            string text;
            var fileStream = new FileStream(Puth, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            using (var streamReader = new StreamReader(fileStream))
            {
                text = streamReader.ReadToEnd();
            }

            return JsonConvert.DeserializeObject<List<Answers>>(text);
        }
    }
}