﻿using System.Collections.Generic;

namespace Questionnare.Web.Dependences
{
    public interface IStatisticsPresenter : IPresenter
    {
        void Init();
    }
}