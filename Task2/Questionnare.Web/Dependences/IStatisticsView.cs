﻿using System.Collections.Generic;

namespace Questionnare.Web.Dependences
{
    public interface IStatisticsView
    {
        IDictionary<int, int> UsersAges { get; set; }
    }
}