﻿namespace Questionnare.Web.Dependences
{
    public interface IResult
    {
        void Show(System.Web.UI.Page page);
    }
}