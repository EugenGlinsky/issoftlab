﻿namespace Questionnare.Web.Dependences
{
    public interface IPresenter
    {
        IResult Result { get; }
    }
}