﻿namespace Questionnare.Web.Dependences
{
    public interface IQuestionnareView
    {
        string FirstName { get; set; }

        string LastName { get; set; }

        string Email { get; set; }

        int Age { get; set; }

        string City { get; set; }

        int Gender { get; set; }

        bool IsPublic { get; set; }
    }
}