﻿using System;
using System.Web.UI;

namespace Questionnare.Web.Dependences
{
    public class RedirectResult : IResult
    {
        private string _nextPage;
        public RedirectResult(string nextPage)
        {
            _nextPage = nextPage;
        }
        public void Show(Page page)
        {
            page.Response.Redirect(_nextPage);
        }
    }
}