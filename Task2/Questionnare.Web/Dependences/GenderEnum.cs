﻿namespace Questionnare.Web.Dependences
{
    public enum GenderEnum
    {
        NoSelected = 0,
        Male = 1,
        Female = 2
    }
}