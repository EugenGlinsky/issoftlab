﻿using System.Data;

namespace Questionnare.Web.Dependences
{
    public interface ISummaryView
    {
        DataTable PrevAnswers { get; set; }
    }
}