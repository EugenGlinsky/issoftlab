﻿using System;
using System.Web.UI;

namespace Questionnare.Web.Dependences
{
    public class ErrorResult : IResult
    {
        public void Show(Page page)
        {
            page.Response.Redirect("~/Page/ErrorPage.aspx");
        }
    }
}