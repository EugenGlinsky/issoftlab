﻿namespace Questionnare.Web.Dependences
{
    public interface IQuestionnarePresenter : IPresenter
    {
        void Save();
    }
}