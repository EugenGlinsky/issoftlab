﻿using Questionnare.Web.Models;
using System.Collections.Generic;

namespace Questionnare.Web.Dependences
{
    public interface ISummaryService
    {
        IEnumerable<Answers> GetAllanswers();
    }
}
