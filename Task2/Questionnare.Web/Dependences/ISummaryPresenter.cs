﻿namespace Questionnare.Web.Dependences
{
    public interface ISummaryPresenter : IPresenter
    {
        void Init();
    }
}