﻿using Questionnare.Web.Models;

namespace Questionnare.Web.Dependences
{
    public interface IQuestionnareService
    {
        bool Save(Answers questionnare);
    }
}
