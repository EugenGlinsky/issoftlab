﻿using Questionnare.Web.Dependences;
using Questionnare.Web.Presenters;
using Questionnare.Web.Services;
using System;
using System.Data;
using System.Web.UI.WebControls;

namespace Questionnare.Web.Pages
{
    public partial class SummaryPage : System.Web.UI.Page, ISummaryView
    {
        private DataTable _prevAnswers;

        public DataTable PrevAnswers
        {
            get
            {
                return _prevAnswers;
            }
            set
            {
                _prevAnswers = value;
                DrawTable();
            }
        }

        private ISummaryPresenter _presenter;

        public SummaryPage()
        {
            _presenter = new SummaryPresenter(this, new SummaryService());
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _presenter.Init();
        }

        private void DrawTable()
        {

            TableRow row = new TableRow();
            if (PrevAnswers == null)
            {
                return;
            }

            for (int i = 0; i < PrevAnswers.Columns.Count; i++)
            {
                TableHeaderCell headerCell = new TableHeaderCell();

                headerCell.Text = PrevAnswers.Columns[i].ColumnName;

                row.Cells.Add(headerCell);
            }
            SummaryTable.Rows.Add(row);

            for (int i = 0; i < PrevAnswers.Rows.Count; i++)
            {
                row = new TableRow();

                for (int j = 0; j < PrevAnswers.Columns.Count; j++)
                {
                    TableCell cell = new TableCell();
                    if (PrevAnswers.Columns[j].ColumnName != "Gender")
                    {
                        cell.Text = PrevAnswers.Rows[i][j].ToString();
                    }
                    else
                    {
                        cell.Text = ((GenderEnum)PrevAnswers.Rows[i][j]).ToString();
                    }
                    row.Cells.Add(cell);
                }
                SummaryTable.Rows.Add(row);
            }
        }
    }
}