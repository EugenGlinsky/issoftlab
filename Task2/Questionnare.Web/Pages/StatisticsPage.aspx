﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="StatisticsPage.aspx.cs" Inherits="Questionnare.Web.Pages.StatisticsPage" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/Chart.min.js" type="text/javascript"></script>
    <link href="../Content/Chart.min.css" type="text/css" rel="stylesheet" />
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentForm" runat="server">

    <div class="row">
        <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 offset-xl-2  offset-md-2 offset-lg-2 bg-light  jumbotron">
            <canvas id="doughnut-chart" width="1000" height="450"></canvas>
        </div>
    </div>

    <script>
        new Chart(document.getElementById("doughnut-chart"), {
            type: 'doughnut',
            data: {
                labels: <%= _years  %>,
                datasets: [
                    {
                        label: "Chart",
                        backgroundColor: <%= _colors%> ,
                        data: <%= _quantity%>
                    }
                ]
            },
            options: {
                title: {
                    display: true,
                    text: 'Age/Quantity chart'
                }
            }
        });
    </script>
</asp:Content>
