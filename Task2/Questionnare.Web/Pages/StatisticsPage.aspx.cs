﻿using Questionnare.Web.Dependences;
using Questionnare.Web.Presenters;
using Questionnare.Web.Services;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;

namespace Questionnare.Web.Pages
{
    public partial class StatisticsPage : System.Web.UI.Page, IStatisticsView
    {
        public IDictionary<int, int> UsersAges { get; set; }

        private IStatisticsPresenter _presenter;

        protected string _years;
        protected string _quantity;
        protected string _colors;


        public StatisticsPage()
        {
            _presenter = new StatisticsPresenter(this, new StatisticsService());
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _presenter.Init();
            SetUsersDataForChart(UsersAges);
        }

        private void SetUsersDataForChart(IDictionary<int, int> usersAges)
        {
            var years = new StringBuilder("[");
            var quantity = new StringBuilder("[");
            var colors = new StringBuilder("[");
            Random random = new Random();
            foreach (var item in usersAges)
            {
                var c = random.Next(0, Int32.MaxValue);
                years.Append($"\"{item.Key} Years\",");
                quantity.Append($"{item.Value},");
                colors.Append($"\"#")
                    .Append(c.ToString("X8"))
                    .Append("\",");
            }
            years.Remove(years.Length - 1, 1)
                .Append("]");
            colors.Remove(colors.Length - 1, 1)
                .Append("]");
            quantity.Remove(quantity.Length - 1, 1)
                .Append("]"); ;

            _years = years.ToString();
            _quantity = quantity.ToString();
            _colors = colors.ToString();
        }
    }
}