﻿using Questionnare.Web.Dependences;
using Questionnare.Web.Presenters;
using Questionnare.Web.Services;
using System;
using System.Web.UI.WebControls;

namespace Questionnare.Pages
{
    public partial class Questionnare : System.Web.UI.Page, IQuestionnareView
    {

        private IQuestionnarePresenter _presenter;

        public Questionnare()
        {
            _presenter = new QuestionnarePresenter(this, new QuestionnareService());
        }

        public int Gender { get => int.Parse(this.GenderDropDownList.SelectedValue); set=> GenderDropDownList.SelectedValue = value.ToString(); }

        public string FirstName { get => FirstNameTextBox.Text; set => FirstNameTextBox.Text = value; }

        public string LastName { get => LastNameTextBox.Text; set => LastNameTextBox.Text = value; }

        public string Email { get => EmailTextBox.Text; set => EmailTextBox.Text = value; }

        public string City { get => CityTextBox.Text; set => CityTextBox.Text = value; }

        public int Age { get => int.Parse(this.AgeTextBox.Text); set => AgeTextBox.Text = value.ToString(); }

        public bool IsPublic { get => this.MakePublicCheckBox.Checked; set => this.MakePublicCheckBox.Checked = value; }
        

        protected void Page_Load(object sender, EventArgs e)
        {
           


        }

        protected void SendButton_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                _presenter.Save();
            }
           
            if(_presenter.Result!=null)
            {
                _presenter.Result.Show(this);
            }
        }

        protected void GenderDropDownList_Load(object sender, EventArgs e)
        {
            var dropDownList = (DropDownList)sender;
            var values = Enum.GetValues(typeof(GenderEnum));
            foreach (var number in values)
            {
                dropDownList.Items.Add(new ListItem(number.ToString(), ((int)number).ToString()));
            }
        }
    }
}