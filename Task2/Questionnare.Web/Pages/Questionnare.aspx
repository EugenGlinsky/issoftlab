﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Questionnare.aspx.cs" Inherits="Questionnare.Pages.Questionnare" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentForm" runat="server">

    <div class="row">
        <div class="col-10 col-sm-10 col-md-5 col-lg-3 col-xl-3 offset-1">
            <div>
                <asp:ValidationSummary runat="server" CssClass="text-danger" ValidationGroup="allFields" ShowSummary="true" DisplayMode="BulletList" />
            </div>
            <label class="text-secondary">First Name</label>
            <asp:TextBox runat="server" ID="FirstNameTextBox" CssClass="form-control"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="FirstNameTextBox" ErrorMessage="First Name is Required" Display="None" ValidationGroup="allFields"> </asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator runat="server" ControlToValidate="FirstNameTextBox" ValidationExpression="[A-Za-z]{2,20}" ErrorMessage="Incorrect format First Name" Display="None" ValidationGroup="allFields"></asp:RegularExpressionValidator>


            <label class="text-secondary">Last Name</label>
            <asp:TextBox runat="server" ID="LastNameTextBox" CssClass="form-control was-validated"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="LastNameTextBox" ErrorMessage="Last Name is Required" Display="None" ValidationGroup="allFields"></asp:RequiredFieldValidator>


            <label class="text-secondary">Email</label>
            <asp:TextBox runat="server" ID="EmailTextBox" CssClass="form-control was-validated"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="EmailTextBox" ErrorMessage="Email is Required" Display="None" ValidationGroup="allFields"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator runat="server" ControlToValidate="EmailTextBox" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ErrorMessage="Invalid format Email" ValidationGroup="allFields" Display="None"></asp:RegularExpressionValidator>

            <label class="text-secondary">Age</label>
            <asp:TextBox runat="server" ID="AgeTextBox" CssClass="form-control was-validated"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="AgeTextBox" ErrorMessage="Age is Required" Display="None" ValidationGroup="allFields"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator runat="server" ControlToValidate="AgeTextBox" ValidationExpression="[1-9]+[0-9]{0,1}" ErrorMessage="Incorrect format Age(from 1 - 99)" Display="None" ValidationGroup="allFields"></asp:RegularExpressionValidator>

            <label class="text-secondary">Sity</label>
            <asp:TextBox runat="server" ID="CityTextBox" CssClass="form-control was-validated"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="CityTextBox" ErrorMessage="Sity is Required" Display="None" ValidationGroup="allFields"></asp:RequiredFieldValidator>

            <label class="text-secondary">Gender</label>
            <asp:DropDownList runat="server" CssClass="form-control was-validated" ID="GenderDropDownList" OnLoad="GenderDropDownList_Load">
            </asp:DropDownList>

            <br />
            <label class="text-secondary">Make results public</label>
            <p>
                <asp:CheckBox runat="server" ID="MakePublicCheckBox" />
            </p>
            <br />
            <asp:Button runat="server" CssClass="btn btn-primary btn-dark" Text="Send" ID="SendButton" CausesValidation="true" ValidationGroup="allFields" OnClick="SendButton_Click" />

        </div>
    </div>
</asp:Content>
