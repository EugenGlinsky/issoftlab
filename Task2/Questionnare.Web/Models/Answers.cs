﻿using Questionnare.Web.Dependences;

namespace Questionnare.Web.Models
{
    public class Answers
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public int Age { get; set; }

        public string City { get; set; }

        public GenderEnum Gender { get; set; }

        public bool IsPublic { get; set; }
    }
}