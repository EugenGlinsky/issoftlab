﻿using Questionnare.Web.Dependences;
using Questionnare.Web.Extensions;
using Questionnare.Web.Models;

namespace Questionnare.Web.Presenters
{
    public class StatisticsPresenter : IStatisticsPresenter
    {
        private IStatisticsView _view;

        private readonly IStatisticsService _service;

        public IResult Result { get; private set; }


        public StatisticsPresenter(IStatisticsView view, IStatisticsService service)
        {
            _view = view;
            _service = service;
        }


        public void Init()
        {
            _view.UsersAges = _service.GetAllUsersYearsAndQuantity();
        }
    }
}