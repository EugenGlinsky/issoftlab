﻿using Questionnare.Web.Dependences;
using Questionnare.Web.Models;

namespace Questionnare.Web.Presenters
{
    public class QuestionnarePresenter : IQuestionnarePresenter
    {
        private IQuestionnareView _view;

        private readonly IQuestionnareService _service;

        public IResult Result { get; private set; }


        public QuestionnarePresenter(IQuestionnareView view, IQuestionnareService service)
        {
            _view = view;
            _service = service;
        }
    

        public void Save()
        {
            var answers = new Answers()
            {
                Age = _view.Age,
                City = _view.City,
                Email = _view.Email,
                FirstName = _view.FirstName,
                Gender = (GenderEnum)_view.Gender,
                LastName = _view.LastName,
                IsPublic=_view.IsPublic
            };

            if (_service.Save(answers))
            {
                Result = new RedirectResult("~/Pages/SummaryPage.aspx");
            }
            else
            {
                Result = new ErrorResult();  
            }
        }
    }
}