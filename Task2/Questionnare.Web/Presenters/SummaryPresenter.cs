﻿using Questionnare.Web.Dependences;
using Questionnare.Web.Extensions;
using Questionnare.Web.Models;

namespace Questionnare.Web.Presenters
{
    public class SummaryPresenter : ISummaryPresenter
    {
        private ISummaryView _view;

        private readonly ISummaryService _service;

        public IResult Result { get; private set; }


        public SummaryPresenter(ISummaryView view, ISummaryService service)
        {
            _view = view;
            _service = service;
        }


        public void Init()
        {
            var prevAnsvers = _service.GetAllanswers();
            _view.PrevAnswers = prevAnsvers.ToDataTable<Answers>();
        }

        
    }
}