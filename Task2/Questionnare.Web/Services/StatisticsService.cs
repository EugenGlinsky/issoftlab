﻿using Questionnare.Web.Dependences;
using Questionnare.Web.Models;
using Questionnare.Web.Repository;
using System.Collections.Generic;

namespace Questionnare.Web.Services
{
    public class StatisticsService : IStatisticsService
    {
      

        public IDictionary<int, int> GetAllUsersYearsAndQuantity()
        {
            var repository = new AnswersRepository();
            var YearsQuantityCollection = new Dictionary<int, int>();
            var allUsers = repository.GetAllAnswers() ?? new List<Answers> ();

            foreach (var user in allUsers)
            {
                var currentUserAge = user.Age;
                YearsQuantityCollection[currentUserAge] = YearsQuantityCollection.ContainsKey(currentUserAge)
                     ? YearsQuantityCollection[currentUserAge]+=1
                     : 1;
            }

            return YearsQuantityCollection;
        }
    }
}