﻿using Questionnare.Web.Dependences;
using Questionnare.Web.Models;
using Questionnare.Web.Repository;
using System.Collections.Generic;

namespace Questionnare.Web.Services
{
    public class SummaryService : ISummaryService
    {
        public IEnumerable<Answers> GetAllanswers()
        {
            var repository = new AnswersRepository();

            return repository.GetAllAnswers();
        }
    }
}