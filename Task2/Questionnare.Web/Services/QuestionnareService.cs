﻿using Questionnare.Web.Dependences;
using Questionnare.Web.Models;
using Questionnare.Web.Repository;

namespace Questionnare.Web.Services
{
    public class QuestionnareService : IQuestionnareService
    {
        public bool Save(Answers questionnare)
        {
            var repository = new AnswersRepository();
            return repository.Save(questionnare);
        }
    }
}